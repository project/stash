<?php

namespace Drupal\Stash\Driver;

use Stash\Interfaces\DriverInterface;

/**
 * The Drupal cache Stash driver.
 */
class Cache implements DriverInterface {

  /**
   * Cache bin to use.
   *
   * @var string
   */
  protected $bin;

  public function setOptions(array $options = array()) {
    $options += $this->getDefaultOptions();

    if (!empty($options['bin'])) {
      $this->bin = $options['bin'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOptions() {
    return array(
      'bin' => 'cache',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getData($key) {
    $cid = static::generateCid($key);
    if ($cache = cache_get($cid, $this->bin)) {
      if ($cache->expires >= REQUEST_TIME) {
        return $cache->data;
      }
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function storeData($key, $data, $expiration) {
    $cid = static::generateCid($key);
    cache_set($cid, $data, $this->bin, $expiration);
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function clear($key = null) {
    if (!isset($key)) {
      cache_clear_all('*', $this->bin, TRUE);
    }
    else {
      $cid = static::generateCid($key);
      cache_clear_all($cid . '*', $this->bin, TRUE);
    }
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function purge() {
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public static function isAvailable() {
    return function_exists('cache_get');
  }

  /**
   * {@inheritdoc}
   */
  public function isPersistent() {
    return true;
  }

  /**
   * Generate a cache ID from a key array.
   *
   * @param array $key
   * @return string
   */
  public static function generateCid(array $key) {
    return implode('::', $key);
  }
}
